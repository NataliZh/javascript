var sum=0;
function sumTo(x1,x2) {
  for (var i = x1; i <= x2; i++) {
    sum += i;
  }
  return sum;
};

var product=0;
function mult(x1,x2) {
	product=x1*x2;
	return product;
};


function buttonClick(){
	document.getElementById('result').innerHTML = '';
	var x1 = parseInt(document.getElementById('x1').value);
	var x2 = parseInt(document.getElementById('x2').value);
    
	var rates = document.getElementsByName('rad');
    var rate_value;
    for(var i = 0; i < rates.length; i++){
    if(rates[i].checked){
        rate_value = rates[i].value;
    }
    }
   
    
	
	if(document.getElementById('x1').value=="" || document.getElementById('x2').value==""){
		alert("Поля x1 и x2 должны быть заполнены");
	} 
	else if(Number.isNaN(x1) || Number.isNaN(x2)) {
		alert("В поляx x1 и x2 должны быть введены числовые значения.");
	} else {
		var resultDiv = document.getElementById('result');
			if(rate_value==1){
			sumTo(x1,x2);
			resultDiv.append("Cумма всех чисел от x1 до x2 " + sum);
			sum=0;
			}
			if(rate_value==2){
			mult(x1,x2);
			resultDiv.append("Произведение x1 и x2 = " + product);
			product=0;
			}				
	}
};

function buttonClear(){
	document.getElementById('x1').value="";
	document.getElementById('x2').value="";
};

